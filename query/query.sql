CREATE TABLE `codeigniter_tutorial`.`users`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NULL,
  `email` varbinary(255) NULL,
  `password` varchar(255) NULL,
  `profile` varchar(255) NULL,
  `deleted` tinyint(1) NULL DEFAULT 0,
  `created_at` datetime(0) NULL,
  `updated_at` datetime(0) NULL,
  PRIMARY KEY (`id`)
);