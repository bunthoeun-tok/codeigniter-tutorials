<div class="panel panel-default">
	<div class="panel-heading">
		<div class="panel-title">
			Create User
		</div>
		<div>
			<?= validation_errors() ?>
		</div>
	</div>
	<div class="panel-body">
		<form action="<?= base_url('users/create') ?>" method="post">
			<div class="form-group">
				<label>Name</label>
				<input type="text" class="form-control" name="name" value="<?= set_value('name'); ?>">
			</div>
			<div class="form-group">
				<label>Email</label>
				<input type="text" class="form-control" name="email" value="<?= set_value('email') ?>">
			</div>
			<div class="form-group">
				<label>Password</label>
				<input type="password" class="form-control" name="password"  value="<?= set_value('password') ?>">
			</div>
			<div class="form-group">
				<label>Profile</label>
				<input type="text" class="form-control" name="profile" value="<?= set_value('profile') ?>">
			</div>

			<div class="form-group d-flex justify-content-end">
				<button class="btn btn-sm btn-primary">Save</button>
			</div>

		</form>
	</div>
</div>