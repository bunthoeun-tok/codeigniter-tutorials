<div class="d-flex mb-4 justify-content-between">
	<h5>Users</h5>
	<a href="<?= base_url('users/create') ?>" class="btn btn-sm btn-info">Create User</a>
</div>

<table class="table table-striped">
	<thead>
        <tr>
            <th>ID</th>
            <th>Name</th>
            <th>Email</th>
            <th>Password</th>
            <th>Profile</th>
            <th style="width: 200px;">Action</th>

        </tr>
	</thead>
	<tbody>
		<?php foreach ($users as $user): ?>
			<tr>
				<td><?= $user->id ?></td>
				<td><?= $user->name ?></td>
				<td><?= $user->email ?></td>
				<td><?= $user->password ?></td>
				<td><?= $user->profile ?></td>
				<td>
					<a class="btn btn-sm btn-warning" href="<?= base_url('users/edit/' .$user->id) ?>">Edit</a>
					<a class="btn btn-sm btn-danger">Delete</a>
				</td>
			</tr>
		<?php endforeach ?>
	</tbody>
</table>