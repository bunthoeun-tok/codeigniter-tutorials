<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class UserModel extends CI_Model {

	public function users()
	{
		return $this->db->select()
			->from('users')
			->get()
			->result();
	}

	public function user($id)
	{
		return $this->db->select()
			->from('users')
			->where('id', $id)
			->get()
			->row();
	}

	public function save($name, $email, $password, $profile)
	{
		$this->db->insert('users', [
			'name' => $name,
			'email' => $email,
			'password' => md5($password),
			'profile' => $profile
		]);
	}

	public function faker($number)
	{
		// Truncate users tables
		$this->db->truncate('users');

		$this->load->library('faker');
		$faker = Faker::create();

		for($index = 0; $index < $number; $index++) {
			$user = [
				'name' 			=> $faker->name,
				'email' 		=> $faker->email,
				'password' 		=> md5('password'),
				'profile' 		=> 'no-image.png',
				'created_at' 	=> date('Y-m-d h:i:s'),
				'updated_at' 	=> date('Y-m-d h:i:s'),
			];
			$this->db->insert('users', $user);
		}
	}

}

/* End of file UserModel.php */
/* Location: ./application/models/UserModel.php */