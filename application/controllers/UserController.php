<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class UserController extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('UserModel', 'user');
		$this->load->library('form_validation');
	}

	public function index()
	{
		$users = $this->user->users();
		$this->load->view('header');
		$this->load->view('users/index', [
			'users' => $users
		]);
		$this->load->view('footer');
	}

	public function show($id)
	{
		$user = $this->user->user($id);
		var_dump ($user);
		die();
	}

	public function create()
	{
		$this->form_validation->set_rules('name', 'name', 'required');
		$this->form_validation->set_rules('email', 'email', 'required');
		$this->form_validation->set_rules('password', 'password', 'required');

		if ($this->form_validation->run() == false) {
            $this->load->view('header');
            $this->load->view('users/create');
            $this->load->view('footer');
        } else {
		    $this->save();
        }
	}

	public function edit($id)
	{
		$user = $this->user->user($id);
        $this->load->view('header');
        $this->load->view('users/edit', [
        	'user' => $user
        ]);
	}

	public function save()
	{
        $name 		= $this->input->post('name');
        $email 		= $this->input->post('email');
        $password 	= $this->input->post('password');
        $profile 	= $this->input->post('profile');

		$this->user->save($name, $email, $password, $profile);
		redirect('users','refresh');
	}

	public function faker($number = 1)
	{
		$this->user->faker($number);

		redirect('users', 'refresh');
	}

/* End of file UserController.php */
/* Location: ./application/controllers/UserController.php */
}